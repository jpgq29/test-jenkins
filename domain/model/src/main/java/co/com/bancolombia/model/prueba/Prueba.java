package co.com.bancolombia.model.prueba;
import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class Prueba {
    public void ejemplo(){
        System.out.println("true = " + true);
    }
}
